package INF102.lab5.graph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public abstract class GraphTest {

    final int N_NODES = 1000;

    Random random = new Random();

    /**
     * Adds edges of all nodes from 0 to N, i.e.:
     * (0 -> 1), (1 -> 2), ..., (N-1 -> N)
     * 
     * @param graph
     */
    public void createConnectedGraph(IGraph<Integer> graph) {
        for (int i = 0; i < N_NODES - 1; i++) {
            graph.addEdge(i, i + 1);
        }
    }

    public void connectedTest(IGraph<Integer> graph) {
        createConnectedGraph(graph);
        int u = 0;
        int v = N_NODES - 1;

        assertTrue(graph.connected(u, v));
    }

    public void notConnectedTest(IGraph<Integer> graph) {
        createConnectedGraph(graph);
        int u = 0;
        int v = N_NODES - 1;
        int w = N_NODES - 2;
        graph.removeEdge(w, v);

        assertFalse(graph.connected(u, v));
    }

    public void addEdgeTest(IGraph<Integer> graph) {
        for (int i = 0; i < N_NODES; i++) {
            int u = random.nextInt(N_NODES);
            int v = random.nextInt(N_NODES);
            graph.addEdge(u, v);
            assertTrue(graph.hasEdge(u, v), "Nodes " + u + " and " + v + " should have an edge between them.");
            assertTrue(graph.hasEdge(v, u), "In an undirected graph " + u + " and " + v + " should have an edge both ways.");
        }
    }

    public void removeEdgeTest(IGraph<Integer> graph) {
        for (int i = 0; i < N_NODES; i++) {
            int u = random.nextInt(N_NODES);
            int v = random.nextInt(N_NODES);
            graph.addEdge(u, v);
            assertTrue(graph.hasEdge(u, v), "Nodes " + u + " and " + v + " should have an edge between them.");
            assertTrue(graph.hasEdge(v, u), "In an undirected graph " + u + " and " + v + " should have an edge both ways.");
            graph.removeEdge(u, v);
            assertFalse(graph.hasEdge(u, v), "Nodes " + u + " and " + v + " should not have an edge between them.");
            assertFalse(graph.hasEdge(v, u), "Nodes " + v + " and " + u + " should not have an edge between them.");
        }
    }

    public void getNeighbourhoodTest(IGraph<Integer> graph) {
        int u = random.nextInt(N_NODES);
        Set<Integer> randomNodes = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            int v = random.nextInt(N_NODES);
            graph.addEdge(u, v);
            randomNodes.add(v);
        }

        for (Integer w : graph.getNeighbourhood(u)) {
            if (!randomNodes.contains(w))
                fail("A neighbour of u was not in the list of neigbhours");
        }
    }

    public void neighbouringNodesAreConnectedTest(IGraph<Integer> graph) {
        for (int i = 0; i < 1000; i++) {
            int u = random.nextInt(N_NODES);
            int v = random.nextInt(N_NODES);
            graph.addEdge(u, v);
            
            assertTrue(graph.connected(u, v));
        }
    }

    public void connectedUndirectedTest(IGraph<Integer> graph) {
        for (int i = 0; i < 1000; i++) {
            int u = random.nextInt(N_NODES);
            int v = random.nextInt(N_NODES);
            graph.addEdge(u, v);
        }

        for (int i = 0; i < 10000; i++) {
            int u = random.nextInt(N_NODES);
            int v = random.nextInt(N_NODES);
            boolean uConnectedToV = graph.connected(u, v);
            boolean vConnectedToU = graph.connected(v, u);

            if (uConnectedToV != vConnectedToU) {
                System.out.println("differing result");    
            }
            boolean aConnectedToV = graph.connected(u, v);
            boolean bConnectedToU = graph.connected(v, u);

            assertEquals(uConnectedToV, vConnectedToU);
        }
    }

}
