package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {
    private int n;

    // --- graph data structure
    private Map<V, Set<V>> map = new HashMap<>();

    @Override
    public int size() {
        return n;
    }

    @Override
    public void addNode(V node) {
        Set<V> neighbours = new HashSet<>();
        map.put(node, neighbours);
        n++;
    }

    @Override
    public void removeNode(V node) {
        if (map.containsKey(node)) {
            map.remove(node);
            n--;
        }
    }

    @Override
    public void addEdge(V u, V v) {
        map.get(u).add(v);
        map.get(v).add(u);
    }

    @Override
    public void removeEdge(V u, V v) {
        map.get(u).remove(v);
        map.get(v).remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        if (map.containsKey(node)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasEdge(V u, V v) {
        if (map.get(u).contains(v) && map.get(v).contains(u)) {
            return true;
        }
        return false;
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return map.get(node);
    }

    /**
     * <p>
     * Checks if node <code>u</code> and node <code>v</code> are connected by a
     * path.
     * </p>
     * 
     * <p>
     * This is an implementation of a Depth First Search, or DFS. It returns true if
     * there
     * is an edge between the two nodes, and false if we visit our target node. The
     * reason we don't have to visit our target node is because we only need to
     * check if there is an edge between it, and the node being passed in.
     * </p>
     * 
     * <p>
     * We have a LinkedList containing the nodes we need to search, and we
     * remove the first node in, and add its neighbours to the queue.
     * </p>
     * 
     * <p>
     * If the set of found nodes contain the node we are currently searching,
     * we omit it, and move on to the next node.
     * </p>
     * 
     * <p>
     * If the node we are currently searching has an edge with the node
     * we are looking for, we return true, otherwise we return false.
     * </p>
     * 
     * <p>
     * To make this a Breadth First Search, we simply change the
     * removeFirst method to removeLast. (DFS operates on a Stack
     * datastructure, and BFS operates on a Queue data structure, i.e
     * LIFO vs FIFO.)
     * </p>
     * 
     * @param V
     * @param u - our start node
     * @param v - our goal node
     */
    @Override
    public boolean connected(V u, V v) {
        if (hasEdge(u, v)) {
            return true;
        }

        HashSet<V> found = new HashSet<>();
        found.add(u);
        LinkedList<V> toSearch = new LinkedList<>(map.get(u));
        V current = u;

        while (!toSearch.isEmpty()) {
            V next = toSearch.removeFirst();

            if (found.contains(next)) {
                continue;
            }

            found.add(next);
            current = next;

            if (hasEdge(current, v)) {
                return true;
            }

            toSearch.addAll(map.get(current));
        }
        return false;
    }
}

// @Override
// public boolean connected(V u, V v) {
//     boolean connected = false;

//     if (hasEdge(u, v)) {
//         return true;
//     }

//     if (visited.get(v)) {
//         return false;
//     }

//     visited.put(u, true);
//     List<V> neighbours = new ArrayList<>(map.get(u));

//     for (V v2 : neighbours) {
//         if (!visited.get(v2)) {
//             connected = connected(v2, v);
//         } 
//     }
//     return connected;
// }
